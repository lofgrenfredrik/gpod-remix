FROM gitpod/workspace-full

# Install Zsh
RUN sudo apt-get install -y zsh

# Set Zsh as the default shell
RUN sudo chsh -s /usr/bin/zsh gitpod